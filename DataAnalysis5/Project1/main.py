n = 1
t = []


def inti(_n):
    pass


def build(a, v: int, tl: int, tr: int):
    if tl == tr:
        t[v] = a[tl]
    else:
        tm = (tl + tr) // 2
        build(a, v * 2, tl, tm)
        build(a, v * 2 + 1, tm + 1, tr)
        t[v] = t[v * 2] + t[v * 2 + 1]


def get_sum(v: int, tl: int, tr: int, l: int, r: int):
    if l > r:
        return 0
    if l == tl and r == tr:
        return t[v]
    tm = (tl + tr) // 2

    return get_sum(v * 2, tl, tm, l, min(r, tm)) + \
           get_sum(v * 2 + 1, tm + 1, tr, max(l, tm + 1), r)


def main():
    arr = [3, 5, 9, 1, 23, 11, 4]
    global n, t
    n = len(arr)
    t = [0] * 4 * n
    build(arr, 1, 0, n - 1)

    print(t)
    print(get_sum(1, 0, n - 1, 2, 5))


if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
